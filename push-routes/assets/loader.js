"use strict";

var cachePresent = document.querySelector(".cache--present");
var cacheIncoming = document.querySelector(".cache--incoming");
var cacheMessage = document.querySelector(".cache-status-message");

function loadApp(e) {
  switch (e.type) {
    case "noupdate":
      cacheMessage.textContent = "Checked Manifest, no update required.";break;

    case "updateready":
      cacheMessage.textContent = "Swapped Cache to Load New Code";break;

    case "cached":
      cacheMessage.textContent = "Finished Caching for the First Time";break;

    default:
      console.log("EVENT TYPE WAS " + e.type);
  }

  console.log("EVENT LOAD", e.type);
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.href = "/assets/app.css";
  var script = document.createElement("script");
  script.src = "/assets/app.js";
  document.body.appendChild(link);
  document.body.appendChild(script);
}

// current version of cache is suitable for running app
window.applicationCache.addEventListener("noupdate", loadApp);
window.applicationCache.addEventListener("error", loadApp);

// application requires refresh to access the new cache
window.applicationCache.addEventListener("updateready", function (e) {
  setTimeout(function () {
    window.applicationCache.swapCache();
    cachePresent.classList.add("is-dismissed");
    cacheIncoming.classList.add("is-swapped");
    loadApp(e);
  }, 2000);
});

window.applicationCache.addEventListener("cached", function (e) {
  setTimeout(function () {
    cachePresent.classList.add("is-dismissed");
    cacheIncoming.classList.add("is-swapped");
    loadApp(e);
  }, 2000);
});

// update visual representation of cache progress
window.applicationCache.addEventListener("progress", function (ev) {
  cacheIncoming.classList.remove("is-to-load");
  if (ev.total) {
    cacheMessage.textContent = "Downloading " + ev.loaded + "/" + ev.total;
  }
});