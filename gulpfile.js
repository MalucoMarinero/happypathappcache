var gulp = require("gulp"),
    using = require("gulp-using"),
    babel = require("gulp-babel"),
    fs = require("fs"),
    connect = require("gulp-connect"),
    less = require("gulp-less");

var SWAP_CACHE_DIR = "./hash-routes";
var PUSH_ROUTE_DIR = "./push-routes";
var ISOMORPHIC_DIR = "./isomorphic";

gulp.task("styles", function() {
  return gulp.src([
    "./styles/loader.less",
    "./styles/app.less",
  ]).pipe(less({
      strictMath: true
    }))
    .pipe(gulp.dest(SWAP_CACHE_DIR + "/assets"))
    .pipe(using({prefix: "Rendered style"}))
    .pipe(gulp.dest(PUSH_ROUTE_DIR + "/assets"))
    .pipe(using({prefix: "Rendered style"}))
    .pipe(gulp.dest(ISOMORPHIC_DIR + "/assets"))
    .pipe(using({prefix: "Rendered style"}))
})

gulp.task("scripts", function() {
  return gulp.src([
    "./scripts/loader.js",
    "./scripts/app.js",
  ]).pipe(babel())
    .pipe(gulp.dest(SWAP_CACHE_DIR + "/assets"))
    .pipe(using({prefix: "Rendered script"}))
})

gulp.task("pushScripts", function() {
  return gulp.src([
    "./scripts/pushLoader.js",
    "./scripts/pushApp.js",
  ]).pipe(babel())
    .pipe(gulp.dest(PUSH_ROUTE_DIR + "/assets"))
    .pipe(using({prefix: "Rendered script"}))
})

gulp.task("isoScripts", function() {
  return gulp.src([
    "./scripts/isoLoader.js",
    "./scripts/isoApp.js",
  ]).pipe(babel())
    .pipe(gulp.dest(ISOMORPHIC_DIR + "/assets"))
    .pipe(using({prefix: "Rendered script"}))
})

gulp.task("default", ["styles", "scripts", "pushScripts", "isoScripts"], function() {
  gulp.watch("./styles/*.less", ["styles"]);
  gulp.watch("./scripts/*.js", ["scripts", "pushScripts", "isoScripts"]);

  connect.server({
    root: SWAP_CACHE_DIR,
    port: 8070
  })
})

gulp.task("push", function() {
  connect.server({
    root: PUSH_ROUTE_DIR,
    port: 8060,
    middleware: function(connect, opt) {
      return [
        function(req, res, next) {
          if (req.url.slice(0,6) == "/link/") {
            fs.readFile(PUSH_ROUTE_DIR + "/routes.html", function(err, data) {
              if (err) throw err

              res.end(data)
            })
          } else {
            next()
          }
        }
      ]
    }
  })
})


gulp.task("iso", function() {
  connect.server({
    root: ISOMORPHIC_DIR,
    port: 8050,
    middleware: function(connect, opt) {
      return [
        function(req, res, next) {
          if (req.url.slice(0,6) == "/link/") {
            fs.readFile(ISOMORPHIC_DIR + "/routes.html", function(err, data) {
              if (err) throw err

              res.end(data)
            })
          } else {
            next()
          }
        }
      ]
    }
  })
})
gulp.task("push", function() {
  connect.server({
    root: PUSH_ROUTE_DIR,
    port: 8060,
    middleware: function(connect, opt) {
      return [
        function(req, res, next) {
          console.log(req.url)
          if (req.url.slice(0,6) == "/link/") {
            fs.readFile(PUSH_ROUTE_DIR + "/routes.html", function(err, data) {
              if (err) throw err

              res.end(data)
            })
          } else {
            next()
          }
        }
      ]
    }
  })
})
