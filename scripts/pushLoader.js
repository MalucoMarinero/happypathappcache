var cachePresent = document.querySelector(".cache--present");
var cacheIncoming = document.querySelector(".cache--incoming");
var cacheMessage = document.querySelector(".cache-status-message");


function loadApp(e) {
  switch (e.type) {
    case "noupdate":
    cacheMessage.textContent = "Checked Manifest, no update required."; break;

    case "updateready":
    cacheMessage.textContent = "Swapped Cache to Load New Code"; break;

    case "cached":
    cacheMessage.textContent = "Finished Caching for the First Time"; break;

    case "noop":
    cacheMessage.textContent = "Not working with Cache at all"; break;

    case "error":
    cacheMessage.textContent = "Could not fetch manifest, use cache."; break;

    default:
    console.log("EVENT TYPE WAS " + e.type);
  }

  console.log("EVENT LOAD", e.type);
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.href = "/assets/app.css";
  var script = document.createElement("script");
  script.src = "/assets/pushApp.js";
  document.body.appendChild(link);
  document.body.appendChild(script);
}

var saveVersion = function() {
  var req = new XMLHttpRequest();
  req.onload = function(e) {
    var VERSION_REGEX = /Version ([\d\.]+)/g;
    var appVersion = VERSION_REGEX.exec(req.responseText)[1];
    
    localStorage.setItem("cacheVersion", appVersion);
    console.log("Saved Version " + appVersion);
  };
  req.open("GET", "/manifest.appcache", true);
  req.send();
};

var versionCheck = function() {
  var req = new XMLHttpRequest();
  req.onload = function(e) {
    var VERSION_REGEX = /Version ([\d\.]+)/g;
    var appVersion = VERSION_REGEX.exec(req.responseText)[1];
    var current = localStorage.getItem("cacheVersion");
    
    console.log("Detected Version " + appVersion + ", currently have " +
                current);

    if (current == null || current != appVersion) {
      var updateIframe = document.createElement("iframe");
      updateIframe.src = "/index.html";
      document.body.appendChild(updateIframe);
    }
  };
  req.open("GET", "/manifest.appcache", true);
  req.send();
};

setTimeout(function() {
  if (window.applicationCache.status == window.applicationCache.UNCACHED) {
    loadApp({type: "noop"})
    versionCheck();
  }
}, 0)



// current version of cache is suitable for running app
window.applicationCache.addEventListener('noupdate', loadApp);
window.applicationCache.addEventListener('error', loadApp);

// application requires refresh to access the new cache
window.applicationCache.addEventListener('updateready', (e) => {
  setTimeout(function() {
    window.applicationCache.swapCache();
    cachePresent.classList.add("is-dismissed");
    cacheIncoming.classList.add("is-swapped");
    loadApp(e);
    saveVersion();
  }, 2000);
});

window.applicationCache.addEventListener('cached', (e) => {
  setTimeout(function() {
    cachePresent.classList.add("is-dismissed");
    cacheIncoming.classList.add("is-swapped");
    loadApp(e);
    saveVersion();
  }, 2000);
});

// update visual representation of cache progress
window.applicationCache.addEventListener('progress', (ev) => {
  cacheIncoming.classList.remove("is-to-load");
  if (ev.total) {
    cacheMessage.textContent = "Downloading " + ev.loaded + "/" + ev.total;
  }
});
