var statusMessage = document.querySelector(".application-status-message");

statusMessage.textContent = "Application Code Loaded.";

var onClickEvent = function(e) {
  var href = e.target.href
  e.preventDefault()
  history.pushState({}, e.target.textContent, href)
}

for (var i = 0; i < 8; i++) {
  var link = document.createElement("a")
  link.href = "/link/" + i
  link.className = "application-link"
  link.textContent = "Push Link " + i
  document.body.appendChild(link)
  link.addEventListener("click", onClickEvent)
}


